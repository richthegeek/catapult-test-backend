defmodule CatapultBackendWeb.CandidateControllerTest do
  use CatapultBackendWeb.ConnCase

  alias CatapultBackend.Candidates
  alias CatapultBackend.Candidates.Candidate

  @create_attrs %{
    candidateName: "some candidateName"
  }
  @update_attrs %{
    candidateName: "some updated candidateName"
  }
  @invalid_attrs %{candidateName: nil}

  def fixture(:candidate) do
    {:ok, candidate} = Candidates.create_candidate(@create_attrs)
    candidate
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all invited_contracts_list", %{conn: conn} do
      conn = get(conn, Routes.candidate_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create candidate" do
    test "renders candidate when data is valid", %{conn: conn} do
      conn = post(conn, Routes.candidate_path(conn, :create), candidate: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.candidate_path(conn, :show, id))

      assert %{
               "id" => id,
               "candidateName" => "some candidateName"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.candidate_path(conn, :create), candidate: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update candidate" do
    setup [:create_candidate]

    test "renders candidate when data is valid", %{conn: conn, candidate: %Candidate{id: id} = candidate} do
      conn = put(conn, Routes.candidate_path(conn, :update, candidate), candidate: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.candidate_path(conn, :show, id))

      assert %{
               "id" => id,
               "candidateName" => "some updated candidateName"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, candidate: candidate} do
      conn = put(conn, Routes.candidate_path(conn, :update, candidate), candidate: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete candidate" do
    setup [:create_candidate]

    test "deletes chosen candidate", %{conn: conn, candidate: candidate} do
      conn = delete(conn, Routes.candidate_path(conn, :delete, candidate))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.candidate_path(conn, :show, candidate))
      end
    end
  end

  defp create_candidate(_) do
    candidate = fixture(:candidate)
    {:ok, candidate: candidate}
  end
end
