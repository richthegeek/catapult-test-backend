defmodule CatapultBackendWeb.ShiftTypeControllerTest do
  use CatapultBackendWeb.ConnCase

  alias CatapultBackend.ShiftTypes
  alias CatapultBackend.ShiftTypes.ShiftType

  @create_attrs %{
    name: "some name"
  }
  @update_attrs %{
    name: "some updated name"
  }
  @invalid_attrs %{name: nil}

  def fixture(:shift_type) do
    {:ok, shift_type} = ShiftTypes.create_shift_type(@create_attrs)
    shift_type
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all role_types", %{conn: conn} do
      conn = get(conn, Routes.shift_type_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create shift_type" do
    test "renders shift_type when data is valid", %{conn: conn} do
      conn = post(conn, Routes.shift_type_path(conn, :create), shift_type: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.shift_type_path(conn, :show, id))

      assert %{
               "id" => id,
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.shift_type_path(conn, :create), shift_type: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update shift_type" do
    setup [:create_shift_type]

    test "renders shift_type when data is valid", %{conn: conn, shift_type: %ShiftType{id: id} = shift_type} do
      conn = put(conn, Routes.shift_type_path(conn, :update, shift_type), shift_type: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.shift_type_path(conn, :show, id))

      assert %{
               "id" => id,
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, shift_type: shift_type} do
      conn = put(conn, Routes.shift_type_path(conn, :update, shift_type), shift_type: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete shift_type" do
    setup [:create_shift_type]

    test "deletes chosen shift_type", %{conn: conn, shift_type: shift_type} do
      conn = delete(conn, Routes.shift_type_path(conn, :delete, shift_type))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.shift_type_path(conn, :show, shift_type))
      end
    end
  end

  defp create_shift_type(_) do
    shift_type = fixture(:shift_type)
    {:ok, shift_type: shift_type}
  end
end
