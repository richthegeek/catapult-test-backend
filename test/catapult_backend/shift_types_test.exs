defmodule CatapultBackend.ShiftTypesTest do
  use CatapultBackend.DataCase

  alias CatapultBackend.ShiftTypes

  describe "role_types" do
    alias CatapultBackend.ShiftTypes.ShiftType

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def shift_type_fixture(attrs \\ %{}) do
      {:ok, shift_type} =
        attrs
        |> Enum.into(@valid_attrs)
        |> ShiftTypes.create_shift_type()

      shift_type
    end

    test "list_role_types/0 returns all role_types" do
      shift_type = shift_type_fixture()
      assert ShiftTypes.list_role_types() == [shift_type]
    end

    test "get_shift_type!/1 returns the shift_type with given id" do
      shift_type = shift_type_fixture()
      assert ShiftTypes.get_shift_type!(shift_type.id) == shift_type
    end

    test "create_shift_type/1 with valid data creates a shift_type" do
      assert {:ok, %ShiftType{} = shift_type} = ShiftTypes.create_shift_type(@valid_attrs)
      assert shift_type.name == "some name"
    end

    test "create_shift_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = ShiftTypes.create_shift_type(@invalid_attrs)
    end

    test "update_shift_type/2 with valid data updates the shift_type" do
      shift_type = shift_type_fixture()
      assert {:ok, %ShiftType{} = shift_type} = ShiftTypes.update_shift_type(shift_type, @update_attrs)
      assert shift_type.name == "some updated name"
    end

    test "update_shift_type/2 with invalid data returns error changeset" do
      shift_type = shift_type_fixture()
      assert {:error, %Ecto.Changeset{}} = ShiftTypes.update_shift_type(shift_type, @invalid_attrs)
      assert shift_type == ShiftTypes.get_shift_type!(shift_type.id)
    end

    test "delete_shift_type/1 deletes the shift_type" do
      shift_type = shift_type_fixture()
      assert {:ok, %ShiftType{}} = ShiftTypes.delete_shift_type(shift_type)
      assert_raise Ecto.NoResultsError, fn -> ShiftTypes.get_shift_type!(shift_type.id) end
    end

    test "change_shift_type/1 returns a shift_type changeset" do
      shift_type = shift_type_fixture()
      assert %Ecto.Changeset{} = ShiftTypes.change_shift_type(shift_type)
    end
  end
end
