defmodule CatapultBackend.CandidatesTest do
  use CatapultBackend.DataCase

  alias CatapultBackend.Candidates

  describe "invited_contracts_list" do
    alias CatapultBackend.Candidates.Candidate

    @valid_attrs %{candidateName: "some candidateName"}
    @update_attrs %{candidateName: "some updated candidateName"}
    @invalid_attrs %{candidateName: nil}

    def candidate_fixture(attrs \\ %{}) do
      {:ok, candidate} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Candidates.create_candidate()

      candidate
    end

    test "list_invited_contracts_list/0 returns all invited_contracts_list" do
      candidate = candidate_fixture()
      assert Candidates.list_invited_contracts_list() == [candidate]
    end

    test "get_candidate!/1 returns the candidate with given id" do
      candidate = candidate_fixture()
      assert Candidates.get_candidate!(candidate.id) == candidate
    end

    test "create_candidate/1 with valid data creates a candidate" do
      assert {:ok, %Candidate{} = candidate} = Candidates.create_candidate(@valid_attrs)
      assert candidate.candidateName == "some candidateName"
    end

    test "create_candidate/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Candidates.create_candidate(@invalid_attrs)
    end

    test "update_candidate/2 with valid data updates the candidate" do
      candidate = candidate_fixture()
      assert {:ok, %Candidate{} = candidate} = Candidates.update_candidate(candidate, @update_attrs)
      assert candidate.candidateName == "some updated candidateName"
    end

    test "update_candidate/2 with invalid data returns error changeset" do
      candidate = candidate_fixture()
      assert {:error, %Ecto.Changeset{}} = Candidates.update_candidate(candidate, @invalid_attrs)
      assert candidate == Candidates.get_candidate!(candidate.id)
    end

    test "delete_candidate/1 deletes the candidate" do
      candidate = candidate_fixture()
      assert {:ok, %Candidate{}} = Candidates.delete_candidate(candidate)
      assert_raise Ecto.NoResultsError, fn -> Candidates.get_candidate!(candidate.id) end
    end

    test "change_candidate/1 returns a candidate changeset" do
      candidate = candidate_fixture()
      assert %Ecto.Changeset{} = Candidates.change_candidate(candidate)
    end
  end
end
