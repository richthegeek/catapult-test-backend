defmodule CatapultBackendWeb.Router do
  use CatapultBackendWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", CatapultBackendWeb do
    pipe_through :api

    resources "/candidates", CandidateController, except: [:new, :edit]
    resources "/shifts", ShiftController, except: [:new, :edit]
    resources "/shift_types", ShiftTypeController, except: [:new, :edit]

    get "/shifts/:id/candidates", ShiftController, :candidates
  end
end
