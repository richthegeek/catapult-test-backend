defmodule CatapultBackendWeb.CandidateController do
  use CatapultBackendWeb, :controller

  alias CatapultBackend.Candidates

  action_fallback CatapultBackendWeb.FallbackController

  def index(conn, _params) do
    candidates = Candidates.list_candidates()
    render(conn, "index.json", candidates: candidates)
  end

  def show(conn, %{"id" => id}) do
    candidate = Candidates.get_candidate!(id)
    render(conn, "show.json", candidate: candidate)
  end

end
