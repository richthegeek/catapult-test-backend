defmodule CatapultBackendWeb.ShiftTypeController do
  use CatapultBackendWeb, :controller

  alias CatapultBackend.ShiftTypes

  action_fallback CatapultBackendWeb.FallbackController

  def index(conn, _params) do
    types = ShiftTypes.list_types()
    render(conn, "index.json", types: types)
  end

  def show(conn, %{"id" => id}) do
    shift_type = ShiftTypes.get_shift_type!(id)
    render(conn, "show.json", shift_type: shift_type)
  end
end
