defmodule CatapultBackendWeb.ShiftController do
  use CatapultBackendWeb, :controller

  alias CatapultBackend.Shifts

  action_fallback CatapultBackendWeb.FallbackController

  def index(conn, params) do
    shifts = Shifts.list_shifts(params)
    render(conn, "index.json", shifts: shifts)
  end

  def show(conn, %{"id" => id}) do
    shift = Shifts.get_shift!(id)
    render(conn, "show.json", shift: shift)
  end

  def candidates(conn, %{"id" => id}) do
    shift = Shifts.get_shift!(id)
    render(conn, "candidates.json", shift: shift)
  end

end