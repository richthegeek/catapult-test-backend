defmodule CatapultBackendWeb.ShiftView do
  use CatapultBackendWeb, :view
  alias CatapultBackendWeb.ShiftView


  def render("index.json", %{shifts: shifts}) do
    %{shifts: render_many(shifts, ShiftView, "shift.json")}
  end

  def render("show.json", %{shift: shift}) do
    %{shift: render_one(shift, ShiftView, "shift.json")}
  end

  def render("shift.json", %{shift: shift}) do
    %{id: shift.id,
      shiftDate: shift.shiftDate,
      startTime: shift.startTime,
      endTime: shift.endTime,
      staff_required: shift.staff_required,
      number_of_invited_staff: length(shift.candidates),
      jobType: shift.jobType.name
    }
  end

  def render("candidates.json", %{shift: shift}) do
    %{
      shift: render("shift.json", shift: shift),
      candidates: render_many(shift.candidates, CatapultBackendWeb.CandidateView, "candidate.json")
    }
  end
end
