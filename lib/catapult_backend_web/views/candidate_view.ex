defmodule CatapultBackendWeb.CandidateView do
  use CatapultBackendWeb, :view
  alias CatapultBackendWeb.CandidateView

  def render("index.json", %{candidates: candidates}) do
    %{candidates: render_many(candidates, CandidateView, "candidate.json")}
  end

  def render("show.json", %{candidate: candidate}) do
    %{candidate: render_one(candidate, CandidateView, "candidate.json")}
  end

  def render("candidate.json", %{candidate: candidate}) do
    %{id: candidate.id,
      candidateName: candidate.candidateName}
  end
end
