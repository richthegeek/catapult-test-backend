defmodule CatapultBackendWeb.ShiftTypeView do
  use CatapultBackendWeb, :view
  alias CatapultBackendWeb.ShiftTypeView

  def render("index.json", %{types: types}) do
    %{types: render_many(types, ShiftTypeView, "shift_type.json")}
  end

  def render("show.json", %{shift_type: shift_type}) do
    %{type: render_one(shift_type, ShiftTypeView, "shift_type.json")}
  end

  def render("shift_type.json", %{shift_type: shift_type}) do
    %{id: shift_type.id,
      name: shift_type.name}
  end
end
