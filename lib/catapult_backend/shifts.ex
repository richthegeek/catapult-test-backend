defmodule CatapultBackend.Shifts do
  @moduledoc """
  The Shifts context.
  """

  import Ecto.Query, warn: false
  alias CatapultBackend.Repo

  alias CatapultBackend.Shifts.Shift
  alias CatapultBackend.ShiftTypes

  @doc """
  Returns the list of shifts, optionally filtered by type and time
  """
  def list_shifts(params) do
    query = from u in Shift

    type = params["type"]

    query = if is_binary(type) do
      type = ShiftTypes.get_by_name(type)
      if !is_nil(type) do
        from u in query, where: u.jobTypeId == ^type.id
      else
        query
      end
    else
      query
    end

    time = params["time"]
    query = cond do
      time == "am" -> from u in query, where: u.startTime < ^"12:00"
      time == "pm" -> from u in query, where: u.startTime >= ^"12:00"
      true -> query
    end

    Repo.all(query)
    |> Repo.preload(:jobType)
    |> Repo.preload(:candidates)
  end

  @doc """
  Returns a single shift by ID
  """
  def get_shift!(id) do
    Repo.get!(Shift, id)
    |> Repo.preload(:jobType)
    |> Repo.preload(:candidates)
  end
end
