defmodule CatapultBackend.Shifts.Shift do
  use Ecto.Schema
  import Ecto.Changeset
  alias CatapultBackend.Repo
  alias CatapultBackend.ShiftTypes.ShiftType
  alias CatapultBackend.Candidates.Candidate

  schema "roles" do
    field :shiftDate, :date
    field :startTime, :time
    field :endTime, :time
    field :staff_required, :integer

    belongs_to :jobType, ShiftType, foreign_key: :jobTypeId
    has_many :candidates, Candidate, foreign_key: :roleId
  end

  @doc false
  def changeset(shifts, attrs) do
    shifts
    |> cast(attrs, [:shiftDate, :startTime, :endTime, :staff_required, :jobType])
    |> validate_required([:shiftDate, :startTime, :endTime, :staff_required, :jobType])
  end

end
