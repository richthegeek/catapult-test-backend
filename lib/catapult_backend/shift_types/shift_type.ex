defmodule CatapultBackend.ShiftTypes.ShiftType do
  use Ecto.Schema
  import Ecto.Changeset

  schema "role_types" do
    field :name, :string
  end

  @doc false
  def changeset(shift_type, attrs) do
    shift_type
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
