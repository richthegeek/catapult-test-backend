defmodule CatapultBackend.ShiftTypes do
  @moduledoc """
  The ShiftTypes context.
  """

  import Ecto.Query, warn: false
  alias CatapultBackend.Repo

  alias CatapultBackend.ShiftTypes.ShiftType

  @doc """
  Returns the list of shift types.
  """
  def list_types do
    Repo.all(ShiftType)
  end

  @doc """
  Gets a single shift_type.
  """
  def get_shift_type!(id), do: Repo.get!(ShiftType, id)

  @doc """
  Gets a single shift_type by name
  """
  def get_by_name(name) do
    Repo.one(from type in ShiftType, where: type.name == ^name)
  end

end
