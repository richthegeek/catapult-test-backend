defmodule CatapultBackend.Repo do
  use Ecto.Repo,
    otp_app: :catapult_backend,
    adapter: Ecto.Adapters.Postgres
end
