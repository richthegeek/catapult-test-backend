defmodule CatapultBackend.Candidates do
  @moduledoc """
  The Candidates context.
  """

  import Ecto.Query, warn: false
  alias CatapultBackend.Repo

  alias CatapultBackend.Candidates.Candidate

  @doc """
  Returns the list of candidates.
  """
  def list_candidates do
    Repo.all(Candidate)
  end

  @doc """
  Returns a single candidate.
  """
  def get_candidate!(id), do: Repo.get!(Candidate, id)
end
