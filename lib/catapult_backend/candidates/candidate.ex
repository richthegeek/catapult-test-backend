defmodule CatapultBackend.Candidates.Candidate do
  use Ecto.Schema
  import Ecto.Changeset
  alias CatapultBackend.Shifts.Shift

  schema "invited_contracts_list" do
    field :candidateName, :string
    belongs_to :shift, Shift, foreign_key: :roleId
  end

  @doc false
  def changeset(candidate, attrs) do
    candidate
    |> cast(attrs, [:roleId, :candidateName])
    |> validate_required([:roleId, :candidateName])
    |> unique_constraint([:roleId, :candidateName])
  end
end
