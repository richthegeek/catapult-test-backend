# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :catapult_backend,
  ecto_repos: [CatapultBackend.Repo]

# Configures the endpoint
config :catapult_backend, CatapultBackendWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "tLWbm/8wW9lufpK/cWbY/YTEOhV2WKyuGohGLoBu4X53yhjjBm8DjWgt7Z/D93Db",
  render_errors: [view: CatapultBackendWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: CatapultBackend.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
