use Mix.Config

# Configure your database
config :catapult_backend, CatapultBackend.Repo,
  username: "postgres",
  password: "postgres",
  database: "catapult_backend_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :catapult_backend, CatapultBackendWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
