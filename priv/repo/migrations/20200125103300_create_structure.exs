defmodule CatapultBackend.Repo.Migrations.CreateStructure do
  use Ecto.Migration

  def change do

    create table(:role_types) do
      add :name, :string
    end
    create unique_index(:role_types, [:name])

    create table(:roles) do
      add :shiftDate, :date
      add :startTime, :time
      add :endTime, :time
      add :staff_required, :integer
      add :jobTypeId, references("role_types")
    end

    create table(:invited_contracts_list) do
      add :roleId, references("roles")
      add :candidateName, :string
    end
    create unique_index(:invited_contracts_list, [:roleId, :candidateName])
  end
end
