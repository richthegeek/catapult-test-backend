alias CatapultBackend.{Repo, Candidate, Shift, ShiftType}

Repo.insert!(%ShiftType{id: 1, name: "Waiting staff"})
Repo.insert!(%ShiftType{id: 2, name: "Retail store shift"})
Repo.insert!(%ShiftType{id: 3, name: "Barista"})
Repo.insert!(%ShiftType{id: 4, name: "Receptionist"})
Repo.insert!(%ShiftType{id: 5, name: "Security"})

Repo.insert!(%Shift{id: 1, shiftDate: ~D[2020-03-10], startTime: ~T[07:00:00], endTime: ~T[14:00:00], staff_required: 5, jobTypeId: 1})
Repo.insert!(%Shift{id: 2, shiftDate: ~D[2020-03-12], startTime: ~T[09:00:00], endTime: ~T[17:00:00], staff_required: 2, jobTypeId: 2})
Repo.insert!(%Shift{id: 3, shiftDate: ~D[2020-03-12], startTime: ~T[13:00:00], endTime: ~T[19:00:00], staff_required: 1, jobTypeId: 1})
Repo.insert!(%Shift{id: 4, shiftDate: ~D[2020-03-13], startTime: ~T[10:00:00], endTime: ~T[18:00:00], staff_required: 3, jobTypeId: 3})
Repo.insert!(%Shift{id: 5, shiftDate: ~D[2020-03-14], startTime: ~T[15:00:00], endTime: ~T[19:00:00], staff_required: 2, jobTypeId: 4})
Repo.insert!(%Shift{id: 6, shiftDate: ~D[2020-03-14], startTime: ~T[10:00:00], endTime: ~T[16:00:00], staff_required: 2, jobTypeId: 5})

Repo.insert!(%Candidate{roleId: 1, candidateName: "Joe Smith"})
Repo.insert!(%Candidate{roleId: 1, candidateName: "Samantha Brown"})
Repo.insert!(%Candidate{roleId: 1, candidateName: "John Doe"})
Repo.insert!(%Candidate{roleId: 2, candidateName: "Frank Jackson"})
Repo.insert!(%Candidate{roleId: 2, candidateName: "Freddie Mercury"})
Repo.insert!(%Candidate{roleId: 3, candidateName: "Joe Smith"})
Repo.insert!(%Candidate{roleId: 3, candidateName: "Samantha Brown"})
Repo.insert!(%Candidate{roleId: 5, candidateName: "Kevin Edwards"})
