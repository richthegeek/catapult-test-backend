# CatapultBackend

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

# About this repo

This is my first time working with Elixir/Phoenix. This does cover the requirements, but I know enough
to know I'm doing it badly. I'm certain there are idiomatic ways of doing things with Phoenix) such as
taking parameters from a GET request and using them to filter a DB) which I have "gone the long way round"
to achieve.

Mostly this feels like I strung together a sufficient number of `mix` commands until things worked. There
is a `test` folder which was auto-generated but I have not made any changes to it as figuring out Elixir
testing would be a larger time commitment than I have available.

## A list of all the shifts
At the endpoint `/api/shifts`, this requires the model to preload the relations to render properly. I assume
there is some way for the controller (or even view) to request that it requires the relations, but I didn't
find the answer to that. I think it's possible to have the model always load the relations on any SELECT with
a sort of 'global preload' but I can't get that to work either.

## A list of all invited contracts
At the endpoint `/api/candidates`. Nothing exceptional here.

## A list of shifts filtered by job type
The endpoint `/api/shifts` supports a `type` parameter, e.g. `/api/shifts?type=Security`. Because we normalise
the shift types into their own model (`/api/shift_types`) it would be natural for the API to support passing
in the ID as well as the Name here, but I can't figure out how to determine if a parameter is numeric. Regex
is an obvious answer but feels extreme for this. Alternatively having a `typeId` parameter would work. If the
type is not known it returns the list as if the filter was not present. Perhaps it should emit an error instead.

## A list of shifts filtered by AM/PM start time
The `/api/shifts` endpoint also supports a `time` parameter which can be `am` or `pm` only. A production API would
*probably* allow direct range filtering on both start and end time.

## A list of candidates invited to a shift
At the endpoint `/api/shifts/:shiftId/candidates` it returns an object containing both the referred-to shift and the
list of candidates.
